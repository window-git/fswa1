<?php

namespace app\uijeasyui\controller;

/**
 * Index 类
 *
 * @since version:1.0(2016-11-4); author:SoChishun(14507247@qq.com); desc:Added.
 */
class Index {
    function index(){
        return view();
    }
}
