[2016-11-28]
# 添加RPM源
rpm -ivh http://mirror.cedia.org.ec/fedora-epel/6/x86_64/epel-release-6-8.noarch.rpm

# 安装必需的组件
yum install git gcc-c++ autoconf automake libtool wget python ncurses-devel zlib-devel libjpeg-devel openssl-devel e2fsprogs-devel sqlite-devel libcurl-devel pcre-devel speex-devel ldns-devel libedit-devel

# 切换目录
cd /usr/src

# git克隆项目
git clone https://freeswitch.org/stash/scm/fs/freeswitch.git

cd /usr/src/freeswitch

# 多线程启动
./bootstrap.sh -j

# 修改配置文件
vi modules.conf

# 编译安装
./configure -C

make && make install

# 安装声音文件
make sounds-install
make moh-install

# 设置拥有者和权限
# create user 'freeswitch'
# add it to group 'daemon'
# change owner and group of the freeswitch installation
cd /usr/local
useradd --system --home-dir /usr/local/freeswitch -G daemon freeswitch
passwd -l freeswitch
 
chown -R freeswitch:daemon /usr/local/freeswitch/ 
chmod -R 770 /usr/local/freeswitch/
chmod -R 750 /usr/local/freeswitch/bin/*
 
mkdir /var/run/freeswitch
chown -R freeswitch:daemon  /var/run/freeswitch
 
ln -s /usr/local/freeswitch/bin/freeswitch /usr/bin/ # needed by /etc/init.d/freeswitch

# 启动
cd /usr/local/freeswitch/bin
./freeswitch

# 设置自动启动
cp /usr/src/freeswitch/build/freeswitch.init.redhat  /etc/init.d/freeswitch
chmod 750 /etc/init.d/freeswitch
chown freeswitch:daemon /etc/init.d/freeswitch
 
chkconfig --add freeswitch && chkconfig --levels 35 freeswitch on